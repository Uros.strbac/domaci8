<?php
    $name = $surname = $choose = "";
    $nameErr = $surnameErr = $chooseErr = "";

    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['surname'])) { $surname = $_GET['surname']; }
    if (isset($_GET['choose'])) { $choose = $_GET['choose']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['surnameErr'])) { $surnameErr = $_GET['surnameErr']; }
    if (isset($_GET['chooseErr'])) { $chooseErr = $_GET['chooseErr']; }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="PHP validation registration">
    <meta name="keywords" content="php, validation, registration">
    <meta name="author" content="Dobrica Rašić">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css" >
    <title>Picture Upload</title>
</head>
<body>

    <h1>Picture upload</h1>

    <form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >
      <fieldset>

        Name: <span class="error">* <?php echo $nameErr;?></span>
        <input type="text" name="name" placeholder="John" value="<?php echo $name;?>"><br>

        Surname: <span class="error">* <?php echo $surnameErr;?></span>
        <input type="text" name="surname" placeholder="Wayne" value="<?php echo $surname;?>"><br>

       Save as:
       <label for="public"><input type="radio" name="choose" id="public" <?php if (isset($choose) && $choose=="public") echo "checked";?>value="public">  Public</label>
       <label for="private"><input type="radio" name="choose" id="private" <?php if (isset($choose) && $choose=="private") echo "checked";?> value="private">Private</label>
        <span class="error">* <?php echo $chooseErr;?></span>
        <br><br>

        <label for="if">Picture Upload:</label>
        <input type="file" name="file" id="if"/><br /><br />
        <input type="submit" name="sb" id="sb" value="upload" />
        <input type="reset" name="rb" id="rb" value="cancel" />

      </fieldset>
    </form>

</body>
</html>
