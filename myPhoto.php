<?php
$nameErr = $surnameErr = $chooseErr = "";
$name = $surname = $choose = $comment = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  }
  else {
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed";
    }
    else {
      $name1 = $_POST['name'];
      if (strlen($name1) <2) {
        $nameErr = "Name must contain at least two characters";
      }
    }
  }

  if (empty($_POST["surname"])) {
    $surnameErr = "Surname is required";
  }
  else {
    $surname = test_input($_POST["surname"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$surname)) {
      $surnameErr = "Only letters and white space allowed";
    }
    else {
      $surname1 = $_POST['surname'];
      if (strlen($surname1) <2) {
        $surnameErr = "Name must contain at least two characters";
      }
    }
  }

  if (empty($_POST["choose"])) {
    $chooseErr = "This is required";
  } else {
    $choose = test_input($_POST["choose"]);
  }

  if (!empty($nameErr) or !empty($surnameErr) or !empty($chooseErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&surname=" . urlencode($_POST["surname"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&surnameErr=" . urlencode($surnameErr);
    $params .= "&genderErr=" . urlencode($genderErr);

    header("Location: index.php?" . $params);
  } else {
    echo "<a href=\"index.php\">Return to form</a>". "<br />";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}



//var_dump($_FILES);
if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {
    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];
    if ($file_error >0) {
        echo "Something went wrong during file upload!";
    }
    else {
        echo exif_imagetype($file_temp)."<br />";
        if (!exif_imagetype($file_temp)) {
          exit("File is not a picture!") ;
        }
        echo "Ime: $file_name <br />";
        echo "Privremena lokacija: $file_temp <br />";
        echo "Veličina: $file_size <br />";
        echo "Tip: $file_type <br />";
        echo "Greška na: $file_error <br />";
        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);


        $file_name_name = strtolower ($_POST['name'])."_".strtolower ($_POST['surname']);
        $new_file_name = $file_name_name. "-" .date("YmdHis") . "-".rand(1, 10). ".$extension";


        $directory = $_POST['choose'];
        $upload = "$directory/$new_file_name"; 
        // upload fajla
        if (!is_dir($directory)) {
          mkdir($directory);
        }
        if (!file_exists($upload)) {
            if (move_uploaded_file($file_temp, $upload)) {
                $size = getimagesize($upload);
                //var_dump($size);
                foreach ($size as $key => $value)
                echo "$key = $value<br />";
                echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";
            }
            else {
              echo "<p><b>Error!</b></p>";
            }
        }
        else {
            for($i=1; $i<9; $i++){
                if(!file_exists($directory.'/'.$file_name_name.$i.'.'.$extension)){
                  move_uploaded_file($file_temp, $directory.'/'.$file_name_name.$i.'.'.$extension);
                  break;
                }
                else{
                  echo "<p><b>File with this name already exists!</b></p>";
                }
            }
        }
    }
}

?>
</body>
</html>
